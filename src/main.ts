import app from "./app";
import "dotenv/config"
import { ErrorRequestHandler,Request,Response,NextFunction } from "express";
const port = process.env.port || process.env.PORT || 3000

import swaggerUI from "swagger-ui-express";
import YAML from 'yamljs';
import { handleHttp } from "./utilities/handleHttp";
const swaggerOptions = YAML.load('./src/routes/v1/swaggerConfig.yaml');

app.use('/v1/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerOptions))

//Error handler
app.use(function(err:any, req:Request, res:Response, next:NextFunction) {    
    handleHttp(res,400,err.message)
});

app.listen(port,() => {
    return console.log(`Server is listening on ${port}`)
})