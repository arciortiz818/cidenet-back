import { Router, Request, Response } from "express";
import { createEmployee, getListEmployees, updateEmployee, getEmployee, deleteEmployee } from "../../employee/employee.controller";
import { getEmployeeByIdentification } from "../../employee/employee.service";

const router = Router()


//Recibe por query params la pagina
router.get('/',getListEmployees)

//Crear un empleado
router.post('/',createEmployee)

//Consultar empleado por id
router.get('/:id',getEmployee)

//Actualizar empleado
router.patch('/:id',updateEmployee)

//Eliminar empleado
router.delete('/:id',deleteEmployee)

export {
    router
}