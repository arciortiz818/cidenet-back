import { Router, Request, Response } from "express";
import { status, countries, areas, identificationTypes } from "../../database/masters";
import { handleHttp } from "../../utilities/handleHttp";

const router = Router()


router.get('/status',(req:Request, res:Response) => {
    handleHttp(res,200,status)
})

router.get('/countries',(req:Request, res:Response) => {
    handleHttp(res,200,countries)
})

router.get('/areas',(req:Request, res:Response) => {
    handleHttp(res,200,areas)
})

router.get('/identification-types',(req:Request, res:Response) => {
    handleHttp(res,200,identificationTypes)
})

export {
    router
}