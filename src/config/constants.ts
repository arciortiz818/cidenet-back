

enum CountriesEnum{
    "COLOMBIA" = "COLOMBIA",
    "ESTADOS UNIDOS" = "ESTADOS UNIDOS"
}

enum IdentificationTypesEnum{
    "CEDULA DE CIUDADANÍA" = "CEDULA DE CIUDADANÍA",
    "CEDULA EXTRANJERIA" = "CEDULA EXTRANJERIA",
    "PASAPORTE" = "PASAPORTE",
    "PERMISO ESPECIAL" = "PERMISO ESPECIAL"
}

enum AreasEnum{
    "ADMINISTRACIÓN" = "ADMINISTRACION",
    "FINANCIERA" = "FINANCIERA",
    "COMPRAS" = "COMPRAS",
    "INFRAESTRUCTURA" = "INFRAESTRUCTURA",
    "OPERACION" = "OPERACION",
    "TALENTO HUMANO" = "TALENTO HUMANO",
    "SERVICIOS VARIOS" = "SERVICIOS VARIOS"
}

const DOMAINS = [
    {"name": "COLOMBIA", "domain": "cidenet.com.co"},
    {"name": "ESTADOS UNIDOS", "domain": "cidenet.com.us"},
]

export {
    CountriesEnum,
    IdentificationTypesEnum,
    AreasEnum,
    DOMAINS
}