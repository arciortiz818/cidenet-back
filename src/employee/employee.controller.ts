import { NextFunction, Request, Response } from "express";
import generateEmail from "../helpers/generateEmail";
import { createEmployeeValidation } from "../helpers/validator";
import { handleHttp } from "../utilities/handleHttp";
import { insertEmployee, getEmployees, getEmployeeByIdentification, getEmployeeById, updateDataEmployee, inactivateEmployee } from "./employee.service";

const getListEmployees = async (req:Request, res:Response) => {
    let page:number = Number(req.query.page)
    const employees = await getEmployees(page);
    handleHttp(res,200,employees)
}

//Crea empleado
const createEmployee = async (req:Request, res:Response, next: NextFunction) => {

    try {
        createEmployeeValidation(req.body)
        
        let { firstName, firstSurname, country, identificationType, identification } = req.body;

        //Verificamos que el empleado no exista
        let existEmployee = await getEmployeeByIdentification(identificationType,identification)
        if(existEmployee.length > 0){
            return handleHttp(res,400,'El empleado ya se encuentra registrado')            
        }

        //Generamos el email
        let emailEmployee:string = await generateEmail({firstName, firstSurname, country})

        //Insertamos el empleado
        await insertEmployee({
            ...req.body,
            email: emailEmployee
        });        
        handleHttp(res,200,'Empleado registrado correctamente.')     
    } catch (error) {
        next(error)
    }

}

const getEmployee = async (req:Request, res:Response) => {
    let employee = await getEmployeeById(req.params.id)
    handleHttp(res,200,employee)     
}

const updateEmployee = async (req:Request, res:Response, next:NextFunction) => {
    try {
        let {admissionDate,...employee} = req.body
        let dataToUpdate = employee

        //Buscamos los datos del empleado actual
        let currentEmployee = await getEmployeeById(req.params.id)

        //Tomamos el email del formulario
        let emailEmployee:string = ''
        //Si primer nombre y primer apellido son diferentes a los que están en la base de datos, generamos de nuevo el correo
        if(currentEmployee?.firstName != employee.firstName.toUpperCase() || currentEmployee?.firstSurname != employee.firstSurname.toUpperCase()){
            
            //Generamos el email
            emailEmployee = await generateEmail({
                firstName:employee.firstName, 
                firstSurname:employee.firstSurname, 
                country:employee.country
            })
            
            dataToUpdate = {
                ...employee, 
                email:emailEmployee
            }
        }

        await updateDataEmployee(req.params.id, dataToUpdate)
        handleHttp(res,200,'Empleado actualizado correctamente.')
    } catch (error) {
        next(error)
    } 
}

const deleteEmployee = async (req:Request, res:Response, next:NextFunction) => {
    try {
        const { id } = req.params
        let deleted = await inactivateEmployee(id)
        if(deleted){
            return handleHttp(res,200,'Empleado eliminado correctamente')
        }
        handleHttp(res,400,'No se pudo eliminar el empleado.')        
    } catch (error) {
        next(error)
    }
}

export {
    getListEmployees,
    getEmployee,
    createEmployee,
    updateEmployee,
    deleteEmployee
}