import "dotenv/config"
const moment = require('moment');

import dbErrorsFormater from "../utilities/dbErrorsFormater";
import { Employee } from "./employee.interface";
import EmployeeModel from "./employee.model";

const insertEmployee = async (employee: Employee) => {    
    try {        
        return await EmployeeModel.create(employee);        
    } catch (error:any) {
        return dbErrorsFormater(error)
    }
}

const getEmployees = async (page:number) => {
    let pageSize = 10
    let skip = (page - 1) * pageSize
    let result = await EmployeeModel.find({state: true}).skip(skip).limit(pageSize).exec()
    let employees = result.map(e => {
        return {
            ...e.toObject(),
            admissionDate: moment(e?.admissionDate).format('YYYY-MM-DD'),            
            registrationDate: moment(e.createdAt).format('YYYY-MM-DD hh:mm:ss a')
        }
    })
    return employees
}

const getEmployeeById = async (id:string) => {
    let employee = await EmployeeModel.findById(id)        
    return {
        ...employee?.toObject(),
        admissionDate: moment(employee?.admissionDate).format('YYYY-MM-DD'),            
        registrationDate: moment(employee?.createdAt).format('YYYY-MM-DD hh:mm:ss a')
    }
}

const getEmployeeByIdentification = async (identificationType:string,identification:string) => {
    return await EmployeeModel.find({
        "identificationType": identificationType,
        "identification": identification
    })    
}

const getEmployeeByEmail = async (firstName:string,firstSurname:string) => {
    let name = `${firstName.replace(/\s+/g,'').toLowerCase()}.${firstSurname.replace(/\s+/g,'').toLowerCase()}`
    return await EmployeeModel.find({"email": new RegExp(name, 'i')})    
}

const updateDataEmployee = async (id:string,employee:Employee) => {
    try {        
        return await EmployeeModel.findByIdAndUpdate(id,employee);        
    } catch (error:any) {
        return dbErrorsFormater(error)
    }
}

const inactivateEmployee = async (id:string) => {
    try {        
        return await EmployeeModel.findByIdAndUpdate(id,{state: false});
    } catch (error:any) {
        return dbErrorsFormater(error)
    }
}

export {
    insertEmployee,
    updateDataEmployee,
    getEmployees,
    getEmployeeByIdentification,
    getEmployeeByEmail,
    getEmployeeById,
    inactivateEmployee
}