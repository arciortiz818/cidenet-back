
import { AreasEnum, CountriesEnum, IdentificationTypesEnum } from "../config/constants";

export interface Employee{
    firstSurname: string,
    secondSurname: string,
    firstName: string,
    otherNames?: string,
    country: CountriesEnum,
    identificationType: IdentificationTypesEnum,
    identification: string,
    email: string,
    admissionDate: Date,
    area: AreasEnum,
    state: Boolean,
    registrationDate: Date,
    createdAt: Date
}
