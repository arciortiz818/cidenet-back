
import { Schema, model } from "mongoose";
import { AreasEnum, CountriesEnum, IdentificationTypesEnum } from "../config/constants";
import { Employee } from "./employee.interface";
const moment = require('moment');

const EmployeeSchema = new Schema<Employee>(
    {
        firstSurname: {
            type: String,
            uppercase: true,
            trim: true,
            required: [true,'El primer apellido es requerido'],
            maxlength: 20,
            match: [/^[A-Za-z\s]+$/g,'El campo contiene caracteres no permitidos']
        },
        secondSurname: {
            type: String,
            uppercase: true,
            trim: true,
            required: [true,'El segundo apellido es requerido'],
            maxlength: 20,
            match: [/^[A-Za-z\s]+$/g,'El campo contiene caracteres no permitidos']
        },
        firstName: {
            type: String,
            uppercase: true,
            trim: true,
            required: [true,'El primer nombre es requerido'],
            maxlength: 20,
            match: [/^[A-Za-z\s]+$/g,'El campo contiene caracteres no permitidos']
        },
        otherNames: {
            type: String,
            uppercase: true,
            trim: true,            
            maxlength: 50,
            match: [/^[A-Za-z\s]+$/g,'El campo contiene caracteres no permitidos']
        },        
        country: {
            type: String,
            enum: CountriesEnum,
            uppercase: true,
            required: [true,'El país es requerido']
        },
        identificationType: {
            type: String,
            enum: IdentificationTypesEnum,
            uppercase: true,
            trim: true,
            required: [true,'El tipo de identificación es requerido']
        },
        identification: {
            type: String,
            uppercase: true,
            trim: true,
            required: [true,'La identificación es requerida'],
            maxlength: 20
        },
        email: {
            type: String,
            lowercase: true,
            unique: true,
            trim: true,
            required: [true, 'El email es requerido'],
            maxlength: 300,
            match: [/.+\@.+\..+/, 'Por favor ingrese un correo válido'] // <- Validación regexp para correo
        },
        admissionDate: {
            type: Date,
            min: [moment().add(-30,'days'),'La fecha de ingreso es inferior a la permitida'],
            max: [moment(),'La fecha de ingreso es superior a la fecha actual']
        },
        area: {
            type: String,
            enum: AreasEnum,
            uppercase: true,
            trim: true,
            required: [true,'El área es requerida']
        },
        state: {
            type: Boolean,
            default: true
        },
    },
    {
        timestamps: true,
        versionKey: false
    }
)


const EmployeeModel = model('employees',EmployeeSchema)

export default EmployeeModel