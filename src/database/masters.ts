//Este archivo exporta los maestros, que se muestran en las listas desplegables

export const areas = [
    "ADMINISTRACIÓN",
    "FINANCIERA",
    "COMPRAS",
    "INFRAESTRUCTURA",
    "OPERACION",
    "TALENTO HUMANO",
    "SERVICIOS VARIOS"
]

export const countries = [
    "COLOMBIA",
    "ESTADOS UNIDOS"
]

export const status = [
    "ACTIVO",
    "INACTIVO"
]

export const identificationTypes = [
    "CEDULA DE CIUDADANÍA",
    "CEDULA EXTRANJERIA",
    "PASAPORTE",
    "PERMISO ESPECIAL"
]