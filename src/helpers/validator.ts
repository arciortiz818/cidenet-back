const createEmployeeValidation = (data:any) => {
    if(!data.identification){
        throw new Error("El número de identificación es requerido"); 
    }
    if(!data.identificationType){
        throw new Error("El tipo de identificación es requerido"); 
    }
    if(!data.firstSurname){
        throw new Error("El número de identificación es requerido"); 
    }
    if(!data.secondSurname){
        throw new Error("El tipo de identificación es requerido"); 
    }
    if(!data.firstName){
        throw new Error("El número de identificación es requerido"); 
    }
    if(!data.country){
        throw new Error("El tipo de identificación es requerido"); 
    }
    if(!data.area){
        throw new Error("El área es requerido"); 
    }
    if(!data.admissionDate){
        throw new Error("El campo fecha de registro es requerido");        
    }
    if(typeof data.firstSurname !== 'string'){
        throw new Error("El campo primer apellido debe ser una cadena de texto");        
    }
    if(typeof data.secondSurname !== 'string'){
        throw new Error("El campo segundo apellido debe ser una cadena de texto");        
    }
    if(typeof data.firstName !== 'string'){
        throw new Error("El campo primer nombre debe ser una cadena de texto");        
    }
    if(data.otherNames && typeof data.otherNames !== 'string'){
        throw new Error("El campo otros nombres debe ser una cadena de texto");        
    }
    if(typeof data.country !== 'string'){
        throw new Error("El campo país debe ser una cadena de texto");        
    }
    if(typeof data.identification !== 'string'){
        throw new Error("El campo identificación debe ser una cadena de texto");        
    }
    if(typeof data.identificationType !== 'string'){
        throw new Error("El campo tipo de identificación debe ser una cadena de texto");        
    }    
}

export {
    createEmployeeValidation
}