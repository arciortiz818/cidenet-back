import { DOMAINS } from "../config/constants"
import { getEmployeeByEmail } from "../employee/employee.service"

export default (data:any):Promise<string> => {
    return new Promise(async (resolve,reject) => {
        let country = DOMAINS.find(e => e.name == data.country.toUpperCase())!
        let tmpEmail = `${data.firstName.replace(/\s+/g,'').toLowerCase()}.${data.firstSurname.replace(/\s+/g,'').toLowerCase()}@${country.domain}`

        //Verificamos si el email generado ya existe, buscando con los nombres
        let existEmail = await getEmployeeByEmail(data.firstName.replace(/\s+/g,'').toUpperCase(), data.firstSurname.replace(/\s+/g,'').toUpperCase())      

        //Si el email generado existe, asignamos el ID secuencial
        if(existEmail.length >= 1){
            let id = existEmail.length
            if(id){
                tmpEmail = `${data.firstName.replace(/\s+/g,'').toLowerCase()}.${data.firstSurname.replace(/\s+/g,'').toLowerCase()}.${id}@${country.domain}`!
            }
        }
        resolve(tmpEmail)
    })
}
