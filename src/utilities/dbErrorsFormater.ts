export default ({errors}:any) => {
    if(errors){
        let messages = []
        let errorKeys = Object.keys(errors)        
        for (let i = 0; i < errorKeys.length; i++) {
            const element = errorKeys[i];
            messages.push({
                [element]: eval(`errors.${element}.message`) 
            })
        }
        return {errors: messages}
    }
    return {}    
}