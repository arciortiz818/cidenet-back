import express,{ Application, ErrorRequestHandler,Request,Response,NextFunction } from "express";

import cors from "cors"
import { router } from "./routes/v1";
import db from "./database/mongodb"

class App{
    public express:Application;

    constructor(){
        this.express = express()  
        this.express.use(express.json());
        this.express.use(express.urlencoded({extended: true}));
        this.express.use(express.static(__dirname + '/public'));        
        
        this.middlewares();
        this.routes();
        this.dbConnect()

    }

    async middlewares(){        
        await this.express.use(cors())
    }

    routes(){
        this.express.use('/api/v1',router)
    }

    async dbConnect(){
        db().then(()=>{
            console.log("Successful connection with MongoDB")
        })
    }
}

export default new App().express