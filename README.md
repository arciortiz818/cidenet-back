# cidenet-back


Esta API REST corresponde al proyecto backend del sistema que administra los empleados.

## Clonar el proyecto

```
git clone https://gitlab.com/arciortiz818/cidenet-back.git
cd cidenet-back
npm install
```

## Ejecutar en modo desarrollo
Para ejecutar el proyecto necesita crear el archivo .env en la raíz del proyecto y configurar las siguientes variables, para el puerto de la aplicación y la url de la base de datos de MongoDB:
```
PORT=
DB_URI=
```

Después de configurado el archivo .env, puede ejecutar el proyecto con el siguiente comando, y la url de la API será por ejemplo si se define el puerto 4000:
- http://localhost:4000/api/v1
```
npm run dev
```

## Documentación de API
En el siguiente enlace podrá encontrar la documentación sobre los endpoints de la API

- http://localhost:4000/api/v1/api-docs